__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

import machine

class DEEPSLEEP:
    def __init__(self):
        self.minutes_to_sleep = 1


    def activate_deepsleep(self):
        machine.deepsleep(self.minutes_to_sleep * 60 * 1000)


if __name__ == "__main__":
    inst_deepsleep = DEEPSLEEP()
    _thread.start_new_thread(inst_deepsleep.activate_deepsleep, ())