__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

from machine import Pin, I2C
import time
import BME280
import _thread

from mqtt_sub_pub import MQTT_PUB


class TEMPPRESSHUMI:
    def __init__(self):
        self.i2c = I2C(scl=Pin(22), sda=Pin(21), freq=10000) #SCL yellow, SDA green
        self.inst_pub = MQTT_PUB()
        self.temp_offset = 2.0

    def read_sensor(self):
        while True:
            bme = BME280.BME280(i2c=self.i2c)
            temp = float(bme.temperature[:5]) - self.temp_offset
            hum = bme.humidity
            pres = bme.pressure
            #print('Temperature: ', temp)
            #print('Humidity: ', hum)
            #print('Pressure: ', pres)
            try:
                self.inst_pub.client_pub.connect()
                self.inst_pub.mqtt_pub(pub_topic="esp32_external/temp1", pub_value=temp)
                time.sleep(1)
                self.inst_pub.mqtt_pub(pub_topic="esp32_external/hum1", pub_value=hum)
                time.sleep(1)
                self.inst_pub.mqtt_pub(pub_topic="esp32_external/pres1", pub_value=pres)
                time.sleep(1)
                self.inst_pub.client_pub.disconnect()
            except:
                self.inst_pub.client_pub.disconnect()
            time.sleep(120)


if __name__ == "__main__":
    inst_sensors = TEMPPRESSHUMI()
    _thread.start_new_thread(inst_sensors.read_sensor, ())
