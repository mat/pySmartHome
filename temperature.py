__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

import esp32
import time
import _thread

from mqtt_sub_pub import mqtt_pub


def pub_celius_temp():
    while True:
        try:
            fahrenheit = esp32.raw_temperature()
            celsius = (int(fahrenheit) - 32) * 5 / 9
            mqtt_pub(pub_topic="esp32_internal", pub_value=celsius)
            time.sleep(10)
        except Exception as e:
            print(e)


if __name__ == "__main__":
    _thread.start_new_thread(pub_celius_temp, ())
